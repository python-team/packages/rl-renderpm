# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/rl-renderpm/issues
# Bug-Submit: https://github.com/<user>/rl-renderpm/issues/new
# Changelog: https://github.com/<user>/rl-renderpm/blob/master/CHANGES
# Documentation: https://github.com/<user>/rl-renderpm/wiki
# Repository-Browse: https://github.com/<user>/rl-renderpm
# Repository: https://github.com/<user>/rl-renderpm.git
